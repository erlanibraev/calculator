package kz.madsoft.calculator.service

import kz.madsoft.calculator.service.impl.OPERATION_MINUS
import kz.madsoft.calculator.service.impl.OperationMinus
import org.junit.Test
import java.math.BigDecimal
import java.util.*
import kotlin.test.assertEquals

open class OperationMinusTest {

    val operation: Operation = OperationMinus()

    @Test
    fun operationMinus() {
        assertEquals(operation.operation(), OPERATION_MINUS)
    }

    @Test
    fun intTest() {
        val op1 = "4"
        val op2 = "2"
        val stack = Stack<BigDecimal>()
        stack.push(BigDecimal(op1))
        stack.push(BigDecimal(op2))
        val result = operation.calculate(stack)
        assertEquals(result.toString(), "2")
    }

    @Test
    fun intNegativeTest() {
        val op1 = "2"
        val op2 = "4"
        val stack = Stack<BigDecimal>()
        stack.push(BigDecimal(op1))
        stack.push(BigDecimal(op2))
        val result = operation.calculate(stack)
        assertEquals(result.toString(), "-2")
    }
}