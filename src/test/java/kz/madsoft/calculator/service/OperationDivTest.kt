package kz.madsoft.calculator.service

import kz.madsoft.calculator.service.impl.OperationDiv
import org.junit.Test
import java.math.BigDecimal
import java.util.*
import kotlin.test.assertEquals

class OperationDivTest {

    val operationDiv = OperationDiv()

    @Test
    fun test() {
        assertEquals(operationDiv.calculate(
                Stack<BigDecimal>().apply {
                    push(BigDecimal(2))
                    push(BigDecimal(3))
                }
        ), BigDecimal(2.0 / 3.0))
    }
}