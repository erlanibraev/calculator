package kz.madsoft.calculator.service

import kz.madsoft.calculator.service.impl.*
import org.junit.Test
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import kotlin.test.assertEquals

class CalculatorServiceTest {

    val calculatorService = CalculatorServiceDefault(
            infixToPolish = InfixToPolishDefault(
                    operations = operations,
                    functions = functions,
                    weightOperation = weightOperation
            ),
            operations = listOf(
                    OperationAdd(),
                    OperationMinus(),
                    OperationDiv(),
                    OperationMult()
            )
    ).apply {
        initOperationsMap()
    }

    @Test
    fun divTest() {
        println(BigDecimal("2").divide(BigDecimal("3"), RoundingMode.HALF_EVEN).toDouble() )
    }

    @Test
    fun calculatorServiceTest() {
        assertEquals(calculatorService.calculate("2 + 3"), BigDecimal(5))

        assertEquals(calculatorService.calculate("2 / 3"), BigDecimal(2.0 / 3.0))
        assertEquals(calculatorService.calculate("-2 / 3"), BigDecimal(-2.0 / 3.0))
        assertEquals(calculatorService.calculate("2 / 3 + (5 - 4)"), BigDecimal(2.0/3.0) + (BigDecimal(5) - BigDecimal(4)))
    }
}