package kz.madsoft.calculator.service

import kz.madsoft.calculator.service.impl.FunctionSqrt
import org.junit.Test
import java.math.BigDecimal
import java.util.*
import kotlin.math.sqrt
import kotlin.test.assertEquals

class FunctionSqrtTest {

    val functionSqrt = FunctionSqrt()

    @Test
    fun test() {
        println(BigDecimal(sqrt(4.0)))
        assertEquals(
                functionSqrt.calculate(
                        Stack<BigDecimal>().apply {
                            push(BigDecimal(4))
                        }
                ), BigDecimal(sqrt(4.0))
        )
    }
}