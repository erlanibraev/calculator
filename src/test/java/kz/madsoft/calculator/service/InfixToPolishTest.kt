package kz.madsoft.calculator.service

import kz.madsoft.calculator.service.impl.InfixToPolishDefault
import kz.madsoft.calculator.service.impl.functions
import kz.madsoft.calculator.service.impl.operations
import kz.madsoft.calculator.service.impl.weightOperation
import org.junit.Test
import kotlin.test.assertEquals

open class InfixToPolishTest {

    val infixToPolish = InfixToPolishDefault(operations = operations, functions = functions, weightOperation = weightOperation)

    @Test
    fun nextOpertionIndex() {
        val str = "(a-b)*(-c+d)";
        println(str)
        var result = infixToPolish.nextOperationIndex(str, 0)
        println(result)
        assertEquals(result,0)
        result = infixToPolish.nextOperationIndex(str, 1)
        println(result)
        assertEquals(result,2)
        result = infixToPolish.nextOperationIndex(str, 2)
        println(result)
        assertEquals(result,2)
        result = infixToPolish.nextOperationIndex(str, 3)
        println(result)
        assertEquals(result,4)
        result = infixToPolish.nextOperationIndex(str, 4)
        println(result)
        assertEquals(result,4)
        result = infixToPolish.nextOperationIndex(str, 5)
        println(result)
        assertEquals(result,5)
        result = infixToPolish.nextOperationIndex(str, 6)
        println(result)
        assertEquals(result,6)
        result = infixToPolish.nextOperationIndex(str, 7)
        println(result)
        assertEquals(result,7)
        result = infixToPolish.nextOperationIndex(str, 8)
        println(result)
        assertEquals(result,9)
        result = infixToPolish.nextOperationIndex(str, 9)
        println(result)
        assertEquals(result,9)
        result = infixToPolish.nextOperationIndex(str, 10)
        println(result)
        assertEquals(result,11)
        result = infixToPolish.nextOperationIndex(str, 11)
        println(result)
        assertEquals(result,11)
        result = infixToPolish.nextOperationIndex(str, 12)
        println(result)
        assertEquals(result,12)
    }

    @Test
    fun prepareTest() {
        val str = "- ( a - - b ) * ( - c + d ) - ( z -  ( p / k ) - - (h +i) )";
        println(str)
        val result = infixToPolish.prepare(str)
        println(result)
        assertEquals(result,"(0-(a-(0-b)))*((0-c)+d)-(z-(p/k)-(0-(h+i)))")
        assertEquals(infixToPolish.prepare("(1+2)-3"), "(1+2)-3")
        assertEquals(infixToPolish.prepare("sqrt-2"), "sqrt(0-2)")
    }

    @Test
    fun rightBracketTest() {

        assertEquals(infixToPolish.findRightBracket("()",0),1)
        assertEquals(infixToPolish.findRightBracket("(((())))",0),7)
        assertEquals(infixToPolish.findRightBracket("(((())))",1),6)
        assertEquals(infixToPolish.findRightBracket("(((())))",2),5)
        assertEquals(infixToPolish.findRightBracket("(((())))",3),4)

        assertEquals(infixToPolish.findRightBracket("((((a))((b))((3)))((444)))",0),"((((a))((b))((3)))((444)))".length-1)
    }

    @Test
    fun getSymbolTest() {
        assertEquals(infixToPolish.getSymbol("a+b",0), "a")
        assertEquals(infixToPolish.getSymbol("a+b",1), "+")
        assertEquals(infixToPolish.getSymbol("a+b",2), "b")
        assertEquals(infixToPolish.getSymbol("-a+b",0), "-")
        assertEquals(infixToPolish.getSymbol("-aaaaa+bbbbbbbbb",1), "aaaaa")
        assertEquals(infixToPolish.getSymbol("(0-(a-(0-b)))*((0-c)+d)(0-)(z-(p/k)(0-)(0-(h+i)))",0),"(")
        assertEquals(infixToPolish.getSymbol("(0-(a-(0-b)))*((0-c)+d)(0-)(z-(p/k)(0-)(0-(h+i)))",1),"0")
        assertEquals(infixToPolish.getSymbol("(0-(a-(0-b)))*((0-c)+d)(0-)(z-(p/k)(0-)(0-(h+i)))",2),"-")
        assertEquals(infixToPolish.getSymbol("(0-(a-(0-b)))*((0-c)+d)(0-)(z-(p/k)(0-)(0-(h+i)))",3),"(")
        assertEquals(infixToPolish.getSymbol("(0-(a-(0-b)))*((0-c)+d)(0-)(z-(p/k)(0-)(0-(h+i)))",11),")")
        assertEquals(infixToPolish.getSymbol("(0-(a-(0-b)))*((0-c)+d)(0-)(z-(p/k)(0-)(0-(h+i)))",13),"*")
    }

    @Test
    fun convertToStringTest() {
        assertEquals(infixToPolish.convertToString("1+2"), " 1 2 +")
        assertEquals(infixToPolish.convertToString("1+2-3"), " 1 2 + 3 -")
        assertEquals(infixToPolish.convertToString("1*2-3"), " 1 2 * 3 -")
        assertEquals(infixToPolish.convertToString("1+2*3"), " 1 2 3 * +")
        assertEquals(infixToPolish.convertToString("1+2/3"), " 1 2 3 / +")
        assertEquals(infixToPolish.convertToString("2/3*5"), " 2 3 / 5 *")
        assertEquals(infixToPolish.convertToString("1+(2-3)"), " 1 2 3 - +")
        assertEquals(infixToPolish.convertToString("(1+2)-3"), " 1 2 + 3 -")
        assertEquals(infixToPolish.convertToString("(1/2)-3"), " 1 2 / 3 -")
        assertEquals(infixToPolish.convertToString("sqrt2"), " 2 sqrt")
        assertEquals(infixToPolish.convertToString("sqrt-2"), " 0 2 - sqrt")
        assertEquals(infixToPolish.convertToString("sqrt(-2*5)"), " 0 2 - 5 * sqrt")
    }

    @Test
    fun convertTest() {
        println(infixToPolish.convert(" 1+ 2"))
        println(infixToPolish.convert(" 1.11+ 2.222 * 5"))
        println(infixToPolish.convert(" sqrt 4"))
    }
}