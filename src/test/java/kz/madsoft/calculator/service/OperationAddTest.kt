package kz.madsoft.calculator.service

import kz.madsoft.calculator.service.impl.OPERATION_ADD
import kz.madsoft.calculator.service.impl.OperationAdd
import org.junit.Test
import java.math.BigDecimal
import java.util.*
import kotlin.test.assertEquals

open class OperationAddTest {

    private val operation: Operation = OperationAdd()

    @Test
    fun operationAdd() {
        assertEquals(OPERATION_ADD, operation.operation())
    }

    @Test
    fun intTest() {
        val op1 = "1"
        val op2 = "2"
        val stack = Stack<BigDecimal>()
        stack.push(BigDecimal(op1))
        stack.push(BigDecimal(op2))
        val result = operation.calculate(stack)
        assertEquals(result.toString(), "3")
    }

    @Test
    fun doubleTest() {
        val op1 = "0.1"
        val op2 = "0.2"
        val stack = Stack<BigDecimal>()
        stack.push(BigDecimal(op1))
        stack.push(BigDecimal(op2))
        val result = operation.calculate(stack)
        assertEquals(result.toString(), "0.3")
    }

    @Test
    fun doubleOtherTest() {
        val op1 = "0.111"
        val op2 = "0.22"
        val stack = Stack<BigDecimal>()
        stack.push(BigDecimal(op1))
        stack.push(BigDecimal(op2))
        val result = operation.calculate(stack)
        assertEquals(result.toString(), "0.331")
    }

    @Test
    fun doubleIntTest() {
        val op1 = "1"
        val op2 = "0.2"
        val stack = Stack<BigDecimal>()
        stack.push(BigDecimal(op1))
        stack.push(BigDecimal(op2))
        val result = operation.calculate(stack)
        assertEquals(result.toString(), "1.2")
    }

}