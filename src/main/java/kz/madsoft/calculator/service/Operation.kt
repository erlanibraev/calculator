package kz.madsoft.calculator.service

import java.math.BigDecimal
import java.util.*

interface Operation {

    fun operation(): String

    fun weight(): Int

    fun calculate(stack: Stack<BigDecimal>): BigDecimal
}