package kz.madsoft.calculator.service.impl

import kz.madsoft.calculator.service.CalculatorService
import kz.madsoft.calculator.service.InfixToPolish
import kz.madsoft.calculator.service.Operation
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*
import javax.annotation.PostConstruct


@Service
class CalculatorServiceDefault(
        val infixToPolish: InfixToPolish,
        val operations: List<Operation>
): CalculatorService {

    lateinit var operationsMap:Map<String, Operation>

    @PostConstruct
    fun initOperationsMap() {
        operationsMap = operations.map{ it.operation() to it}.toMap()
    }

    override fun calculate(expression: String): BigDecimal {
        val stack = infixToPolish.convert(expression)
        val result = Stack<BigDecimal>()
        while(!stack.isEmpty()) {
            val symbol = stack.pop()
            if(!operationsMap.keys.contains(symbol)) {
                result.push(BigDecimal(symbol))
            } else {
                val calculate = operationsMap[symbol]!!.calculate(result)
                result.push(calculate)
            }
        }
        return result.pop()
    }
}