package kz.madsoft.calculator.service.impl

import kz.madsoft.calculator.service.Function
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.util.*
import kotlin.math.sqrt

const val FUNCTION_SQRT = "SQRT"

@Component(FUNCTION_SQRT)
class FunctionSqrt: Function {
    override fun weight(): Int = 2

    override fun operation(): String = "sqrt"

    override fun calculate(stack: Stack<BigDecimal>): BigDecimal {
        val op = stack.pop()
        return BigDecimal(sqrt(op.toDouble()))
    }

}