package kz.madsoft.calculator.service.impl

import kz.madsoft.calculator.service.Operation
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.util.*

const val OPERATION_MULT = "MULT"

@Component(OPERATION_MULT)
open class OperationMult: Operation {

    override fun weight(): Int = 1

    override fun operation(): String = "*"

    override fun calculate(stack: Stack<BigDecimal>): BigDecimal {
        val op1 = stack.pop()
        val op2 = stack.pop()
        return op1 * op2
    }
}