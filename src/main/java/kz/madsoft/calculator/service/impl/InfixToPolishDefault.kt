package kz.madsoft.calculator.service.impl

import kz.madsoft.calculator.service.InfixToPolish
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import java.lang.Exception
import java.util.*
val functions = setOf("sqrt")
val weightOperation = mapOf("*" to 1, "/" to 1, "+" to 2, "-" to 2)
val operations = setOf("*", "/", "+", "-", "(", ")") + functions


@Component
class InfixToPolishDefault(
        @Qualifier("operations")val operations: Set<String>,
        @Qualifier("functions") val functions: Set<String>,
        @Qualifier("weightOperation") val weightOperation: Map<String, Int>

): InfixToPolish {

    override fun convert(expression: String): Stack<String> {
        var stack = Stack<String>()
        convertToString(expression).split(" ").filter{!it.isEmpty()}.reversed().forEach { stack.push(it) }
        return stack
    }

    fun convertToString(expression: String): String {
        var result = ""
        val prepare = prepare(expression)
        val stack = Stack<String>()

        var index = 0
        while(index < prepare.length) {
            val symbol = getSymbol(prepare, index)
            if (!operations.contains(symbol)) {
                result += " $symbol"
            } else if(functions.contains(symbol)) {
                stack.push(symbol)
            } else if(symbol == "(") {
                stack.push(symbol)

            } else if(symbol == ")") {
                while(stack.peek() != "(") {
                    result += " ${stack.pop()}"
                }
                if(stack.empty()) throw Exception("Wrong expression")
                stack.pop()
            } else if(operations.contains(symbol)) {
                if(!stack.isEmpty()) {
                    while ( !stack.isEmpty() &&  stack.peek() != "(" && (  !operations.contains(stack.peek()) || weightOperation[stack.peek()]?:0 <= weightOperation[symbol]?:0)) {
                        result += " ${stack.pop()}"
                    }
                }
                stack.push(symbol)
            }
            index += symbol.length
        }
        while(!stack.empty()) {
            result += " ${stack.pop()}"
        }

        return result
    }

    fun prepare(expression: String): String =
            expression.replace(" ","").toLowerCase().let { expression ->
                var index = 0
                var result = ""
                var prevSymbol = ""
                while(index < expression.length) {
                    var symbol = getSymbol(expression, index)
                    if(symbol == "-") {
                        if(index == 0 || (operations.contains(prevSymbol) && prevSymbol != ")")) {
                            val nextSymbolIndex = index + symbol.length
                            val nextSymbol = getSymbol(expression, nextSymbolIndex)
                            if(nextSymbol =="(") {
                                val rightBracketIndex = findRightBracket(expression, nextSymbolIndex)
                                val middle = expression.substring(index+symbol.length, rightBracketIndex+1)
                                result += "(0-"+prepare(middle) +")"
                                index = rightBracketIndex+1
                                symbol = ""
                            } else {
                                result += "(0-"+nextSymbol+")"
                                index = nextSymbolIndex + nextSymbol.length
                                symbol = nextSymbol
                            }
                        } else {
                            result += symbol
                            index += symbol.length
                        }
                    } else {
                        result += symbol
                        index += symbol.length
                    }
                    prevSymbol = symbol
                }

                result
            }

    fun nextOperationIndex(expression: String, start: Int): Int {
        val test = expression.substring(start)
        val operationIndex = operations.map {
            val index = test.indexOf(it)
            if (index >=0) index + start else index
        }.filter { it >= 0 }
        return operationIndex.min() ?: test.length + start
    }

    fun findRightBracket(expression: String, startLeftBracket: Int): Int {
        var openBracket = 1
        var index = startLeftBracket
        while(openBracket > 0) {
            index++
            if(expression[index] == '(') openBracket++
            if(expression[index] == ')') openBracket--
        }
        return index
    }

    fun getSymbol(expression: String, startIndex: Int): String {
        var index = nextOperationIndex(expression, startIndex)
        if(index == startIndex) {
            val op = operations.find { expression.substring(startIndex,startIndex + it.length) == it }
            if(op.isNullOrEmpty() ) {
                index = nextOperationIndex(expression, startIndex + 1)
            } else {
                index = startIndex + op.length
            }
        }
        val result = expression.substring(startIndex, index)
        return result
    }
}