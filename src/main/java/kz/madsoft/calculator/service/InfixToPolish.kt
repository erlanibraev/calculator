package kz.madsoft.calculator.service

import java.util.*

interface InfixToPolish {
    fun convert(expression: String): Stack<String>
}