package kz.madsoft.calculator.service

import java.math.BigDecimal

interface CalculatorService {

    fun calculate(expression: String): BigDecimal

}