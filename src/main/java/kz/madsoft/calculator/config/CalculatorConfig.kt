package kz.madsoft.calculator.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["kz.madsoft.calculator"])
open class CalculatorConfig {

    @Bean(value = ["weightOperation"])
    open fun weightOperation(): Map<String,Int> =
            kz.madsoft.calculator.service.impl.weightOperation

    @Bean(value=["operations"])
    open fun operations(): Set<String> =
            kz.madsoft.calculator.service.impl.operations

    @Bean
    open fun functions(): Set<String> =
            kz.madsoft.calculator.service.impl.functions

}